# Spinni in Amerika

## Zusammenfassung

Spinni fährt mit großen Hoffnungen nach Amerika. Leider werden sie alle enttäuscht. In seltsam trippiger Atmosphäre passieren seltsame Sachen. Am Ende stellt sich raus, dass alles ein Fiebertraum war.

## Storyline

- Spinni hat ein Problem: Er hat kein eigenes Geld
    - Wie wird es akut?
        - Im Restaurant (oder andere regelmäßige, öffentliche Tätigkeit) hat Spinni immer nur das Kinderprodukt gekriegt. Irgendwann trifft er dort Weylo, der sich alles aussuchen kann, und Spinni fragt, warum er das nicht auch tut (vielleicht ein besonders "erwachsenes" Menü mit Alkohol oder so)
- in einem Büro liest das Gegenüber seinen Lebenslauf – ihm wird mitgeteilt, dass er ohne Schulabschluss keine Chance hat
- er veruschts beim Drogenhändler – am Ende verlangt selbst der einen Schulabschluss

- Spinni sieht in einem Fernseher (vielleicht holt Drogenhändler ein Video? "wir sind hier doch nicht in Amerika" "was ist Amerika?") eine Reklame, in der ein Mensch (!) verspricht, dass im Land der unbegrenzten Möglichkeiten alles aus einem werden kann.
- Spinni entschließt, in dieses Land auszuwandern.

- Oma Vielbein merkt, dass sie von Spinni abhängig ist, und will nicht, dass er geht. 
- Spinni bleibt hartnäckig und möchte alleine auswandern.
- Während Spinni packt, macht Oma Vielbein sich andauernd Sorgen, ob er denn alles eingepackt hat.
- Dann kommt der Tag, an dem Spinni fliegt.
    - Zuerst sieht man nur ein Flugzeug von innen: Ein ganz normaler Tag, Steward geht rum, viele Gäste
    - plötzlich sieht man durch Fenster Oma Vielbein angehäkelt kommen, die ruft, dass Spinni irgendwas vergessen hat
    - Oma Vielbein wird von der Flugzeugturbine aufgesaugt, das Flugzeug stürzt ab
    - Oma Vielbein wächst am Flughafen nach, man sieht, dass Spinni in ein anderes Flugzeug einsteigt
    - in Spinnis Flugzeug taucht Oma Vielbein dann einfach auf (sie hat sich offenbar mit reingeschlichen)
- In Amerika angekommen, ist Spinni ganz außer sich, und erzählt begeistert von den Möglichkeiten – sie probieren, während Spinni es im Off erzählt, direkt alles aus
    - Hot Dogs essen
    - NY city stuff
- im letzten Shot fragt Oma Vielbein (mit ganz vielen Souvenirs) dann, wie Spinni an Geld kommen möchte (nachdem sie nur Geld ausgegeben haben)
- Wird aber unterbrochen: Oma Vielbein wird von Polizisten entdeckt und mit einem Maschinengewehr niedergeschossen, weil sie eine andere Farbe hat.
- Nach einer Woche sind beide Obdachlos und leben unter einer Brücke. Oma Vielbein wird nach wie vor regelmäßig abgeknallt, wenn sie von Polizisten gesehen wird.


(
- sind krank
- gehen zum Doc
- Doc ist Drogenhändler
)

(
- versuchen an Geld zu kommen
    - Idee 1
        - nach langem Betteln kaufen Spinni und Oma Vielbei einen überteuerten Burger, der aber deutlich kleiner ist als in der Werbung
        - abgefuckt dadurch verkaufen sie Oma-Vielbein-Leichen als Essen
    - Idee 2
        - was Mia machen würde, um in NY an Geld zu kommen
- sie machen damit Geld, das ihnen direkt wieder geklaut wird – oder sie für Drogen rausschmeißen müssen
)

- sie finden einen Weg, an ein Wolle-Äquivalent zu kommen, und wollen damit fliehen
    - Wolle-Äquivalent:
        - Seile der Golden Gate Bridge?
- bei der Flucht sprengen sie Amerika hinter sich in die Luft (der "einzige moralisch vertretbare Weg")
- Wolle reicht nicht, weil Spinni sich verrechnet hat (Punkt vor Strich hatten sie noch nicht)
- Sie stürzen ab, und landen wieder in Amerika?

- Irgendwann hält ein Auto neben Spinni an und kurbelt das Fenster runter. Im Auto sitzt der Mensch aus der Fernsehwerbung.
- Der Animationsstil hat sich subtil verändert.Bis auf Spinni und den Menschen mit seinem Auto sind die Straßen inzwischen leer, die Fenster in den Gebäuden sind dem Animationsstil entsprechend lila und undurchsichtig.
- Der Mensch aus dem Fernseher sagt Spinni, das Geld würde auf der Straße liegen, man müsse nur die Hand ausstrecken.

- Das Geld liegt nun tatsächlich auf der Straße, ist für Spinni aber nicht erreichbar (was irgendeinen cartoon-physikalischen Grund haben muss).
    - Welche cartoon-physikalischen Gründe könnten dazu führen, dass Spinni das Geld auf der Straße nicht erreichen kann?
- Beim Versuch, dem Geld hinterherzujagen, kommt der Mensch regelmäßig aus Gullis, Hydranten etc. uns fragt Spinni, ob er es denn schon geschafft hätte, und sagt ihm, dass er selbst daran schuld wäre dass er es nicht schaffen würde.
- Der Mensch wird immer wütender und penetranter (aber irgendwie uncanny, also er sollte die Emotion komisch zeigen, zum Beispiel gar nicht im Gesicht).
- Irgendwann beißt er Spinni.
    - Vielleicht glitcht das Gebiss komisch aus seinem 3D-Körper und kommt immer näher und beißt ihn dann?
    - Szene vielleicht ohne Sound, wie so ne 70er-CGI-Tech-Demo?

- Jetzt dreht sich das Bild um Spinni herum, der Animationsstil ändert sich abermals. Spinni hat plötzlich ein gezeichnetes Spinnennetz auf dem Rücken. - Es kommt eine Musik und eine Offstimme, die wirken wie aus der Zeit, als es die ersten Spiderman-Cartoons (oder Batman-Cartooons?) gab.
- Die Offstimme präsentiert Spinni Vielbein, der nach dem Biss durch einen Menschen zur Spider-Spinne wurde und von nun an Gotham City beschützt. Spinni klettert dabei über Häuserdächer und hat ein wehendes Superman-Cape an.

...

- Spinni wacht auf, wo er ohnmächtig geworden ist (beim Drogenhändler? Zuhause?)
- Es stellt sich raus, dass es "Amerika" gar nicht gibt auf dem Spinnenplaneten (alle Länder haben "Spinne" im Namen: Spinnland, Spinnien, Spinnonien, Spinnenreich, Spinna)


